FROM python:3.7-alpine

RUN adduser -D pat

WORKDIR /home/pat

# Postgres dependencies
RUN apk add --no-cache --virtual .build-dependencies gcc musl-dev \
    postgresql-libs postgresql-dev
RUN apk add libpq

# Python dependencies
COPY requirements.txt requirements.txt
RUN python -m venv venv
RUN venv/bin/pip install -r requirements.txt --no-cache-dir
RUN venv/bin/pip install gunicorn psycopg2

# Remove build dependencies
RUN apk --purge del .build-dependencies

# Copy app files
COPY app app
COPY migrations migrations
COPY pat.py config.py boot.sh .flaskenv ./
RUN chmod +x boot.sh

RUN chown -R pat:pat ./
USER pat

ENTRYPOINT ["./boot.sh"]
