FLASK_APP=pat.py
FLASK_DEBUG=1
FLASK_ENV=development
MAIL_SERVER={{smtp server of email used for sending error logs}}
MAIL_PORT=25
MAIL_USE_TLS=1
MAIL_USERNAME={{username of email used for sending error logs}}
MAIL_PASSWORD={{password of email used for sending error logs}}
