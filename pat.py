from flask import request

from app import create_app

app = create_app()


from app.account.role import Role
@app.context_processor
def context_processor():  # Make common data available to all templates for simplicity
    return dict(Role=Role, request=request)
