Dobrý den,

terapeut {{ therapist.username }} s Vámi chce navázat terapeutický vztah v naší aplikaci.

Navázáním terapeutického vztahu budou Vaše účty v aplikaci propojeny. Terapeut uvidí Vaše
záznamy automatických myšlenek a výsledky Vašich testů. Po dobu propojení zároveň uvidí Vaši
emailovou adresu. Toto propojení je možné kdykoliv zrušit v sekci 'Úprava profilu'.

Nepoznáváte-li tohoto terapeuta, doporučujeme pozvání nepřijímat.

Pro přijetí navázání terapeutického vztahu klikněte na následující odkaz:

{{ url_for('client.accept_alliance', token=token, _external=True) }}

Z bezpečnostních důvodů je odkaz platný jen po dobu {{ token_expiration_string }}.

Pokud si nepřejete navázání přijmout, doporučujeme tento email smazat.

Tato zpráva je automatizovaná, neodpovídejte na ni prosím.

{% include "email_templates/_signature.txt" %}
