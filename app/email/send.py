import socket
from threading import Thread
from smtplib import SMTPException

from flask import current_app
from flask_mail import Message

from app import mail


def _send_email(message):
    """Wrapper for sending emails. Also catches and logs email errors."""
    try:
        mail.send(message)
    except (SMTPException, socket.gaierror) as error:
        current_app.logger.error(error)


def _send_email_async(message):
    """Helper function for async sending."""
    def _send(app, mess):
        with app.app_context():
            _send_email(mess)
    Thread(target=_send, args=[current_app._get_current_object(), message]).start()


def send_email(subject, recipients, html_body, text_body, asynchronous=True, **kwargs):
    """General function for sending emails from the app. Uses threads for async sending."""
    message = Message(subject=subject, recipients=recipients,
                      html=html_body, body=text_body, **kwargs)
    if asynchronous:
        _send_email_async(message)
    else:
        _send_email(message)
