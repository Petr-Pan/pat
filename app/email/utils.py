"""Module for email helper functions."""


def czech_expiration_string(seconds):
    """Helper function for generating token expiration strings for Czech emails.
    Behaviour:
        - Weeks are the highest supported order.
        - Currently only 2nd declension is supported.
        - This function rounds down to the highest order:
            _expiration_string(119) == _expiration_string(60) == "1 minuty"
    """
    order_values = [60, 60, 24, 7]
    order = 0
    remainder = seconds
    for value in order_values:
        if value % (remainder + 1) == value:  # Add 1 to remainder so that fn(60) returns "1 minuty" and not "60 vteřin"
            order += 1
            remainder //= value
        else:
            break

    last_digit = str(remainder)[-1]
    if last_digit == "1":
        order_strings = ['vteřiny', 'minuty', 'hodiny', 'dne', 'týdne']
    else:
        order_strings = ['vteřin', 'minut', 'hodin', 'dnů', 'týdnů']

    return str(remainder) + " " + order_strings[order]
