"""This module is the app wrapper for sending emails via Flask-Mail."""

from flask import Blueprint

bp = Blueprint('email', __name__)

from app.email import send
