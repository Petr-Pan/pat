from config import FieldLengths


class FormErrorMsg:
    """Constructor for repeating error strings in form validations."""
    @staticmethod
    def max_length_error(max_length):
        return f"Maximální délka tohoto pole je {max_length} znaků."

    # User related error messages
    class Username:
        Required = "Zadejte uživatelské jméno"
        MaxLength = f"Maximální limit délky uživatelského jména je {FieldLengths.User.Max.Username} znaků."

    class Email:
        Required = "Zadejte email."
        Invalid = "Neplatný email."
        Duplicate = "Tento email je již zaregistrovaný."
        MaxLength = f"Maximální limit délky emailu je {FieldLengths.User.Max.Email} znaků."

    class Password:
        Required = "Zadejte heslo."
        NotEqual = "Zadaná hesla se neshodují."
        MaxLength = f"Maximální limit délky hesla je {FieldLengths.User.Max.Password} znaků."
        MinLength = f"Minimální limit délky hesla je {FieldLengths.User.Min.Password} znaků."
        EqualToUsername = "Uživatelské jméno nemůže být použito jako heslo."
        EqualToEmail = "Email nemůže být použit jako heslo."

    # ThoughtRecord related error messages
    class Date:
        Required = "Chybí platné datum."

    class Time:
        Required = "Chybí platný čas."

    class ThoughtRecord:
        Empty = "Prázdný záznam nelze uložit."

    # Test related error messages
    class RadioField:
        Required = "Tato položka je povinná."

    # Message related error messages
    class Farewell:
        Required = "Nelze odeslat prázdnou zprávu."
        MaxLength = f"Omlouváme se, ale kvůli technickým omezením je maximální délka zprávy  \
                      {FieldLengths.Message.Max.Farewell} znaků. Pro delší zprávu prosím použijte \
                      emailovou komunikaci s daným terapeutem"

    # User support related error messages
    class UserSupportMail:
        Required = "Nelze odeslat prázdnou zprávu."
        MaxLength = f"Omlouváme se, ale kvůli technickým omezením je maximální délka zprávy \
                      {FieldLengths.Message.Max.UserSupportMail} znaků."

    class UserSupportMailSubject:
        MaxLength = f"Maximální povolená délka předmětu je {FieldLengths.Message.Max.UserSupportMailSubject} znaků."
