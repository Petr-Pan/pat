"""This module provides error related functionality, e.g. handling and displaying errors to the users."""

from flask import Blueprint

bp = Blueprint('errors', __name__)

from app.errors import handlers
