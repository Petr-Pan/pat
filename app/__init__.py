import logging

from flask import Flask
from flask_bootstrap import Bootstrap
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
from flask_login import LoginManager
from flask_mail import Mail
from flask_migrate import Migrate
from flask_moment import Moment
from flask_sqlalchemy import SQLAlchemy
from werkzeug.middleware.proxy_fix import ProxyFix

from config import Config


db = SQLAlchemy()
migrate = Migrate()
limiter = Limiter(key_func=get_remote_address)
login = LoginManager()
login.login_view = 'account.login'
login.login_message = 'Tato stránka je přístupná pouze přihlášeným uživatelům.'
mail = Mail()
moment = Moment()
bootstrap = Bootstrap()


def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(config_class)

    app.jinja_env.trim_blocks = True  # Else Jinja control blocks leave whitespace when used

    db.init_app(app)
    migrate.init_app(app, db)
    limiter.init_app(app)
    login.init_app(app)
    mail.init_app(app)
    moment.init_app(app)
    bootstrap.init_app(app)

    from app.account import bp as account_bp
    app.register_blueprint(account_bp)

    from app.client import bp as client_bp
    app.register_blueprint(client_bp)

    from app.common import bp as common_bp
    app.register_blueprint(common_bp)

    from app.core import bp as core_bp
    app.register_blueprint(core_bp)

    from app.errors import bp as errors_bp
    app.register_blueprint(errors_bp)

    from app.therapist import bp as therapist_bp
    app.register_blueprint(therapist_bp, url_prefix='/therapist')

    if not app.debug and not app.testing:
        from app.core.loggers import init_email_logging, init_file_logging
        if app.config['MAIL_SERVER']:
            init_email_logging(app)
        init_file_logging(app)

        app.logger.setLevel(logging.INFO)
        app.logger.info('PAT startup')

        for handler in app.logger.handlers:
            limiter.logger.addHandler(handler)

        app.wsgi_app = ProxyFix(app.wsgi_app, x_for=1)  # Limiter - fix get_remote_address behavior if the app is behind a reverse proxy

    return app
