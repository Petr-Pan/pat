"""
Python implementation of psychological tests.

How to add a test:
Create a class similar to the existing tests. The form and template can be test specific,
however, they need to be WTForms compatible. The test is then to be added to test_selector.TestSelector.tests.
Keys of this dictionary are used to look up the test in routes and database - ensure
the new key is unique.

As of now, there is no option to choose a test client-side -> clients use a predetermined test.
Adding a new test now therefore requires an implementation of client-side test picking.
Saving and reading tests should already be dynamic.
"""
