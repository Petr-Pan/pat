from app.tests.cesd_r import CESD_R


class TestSelector:
    tests = {
            CESD_R.label: CESD_R,
        }

    @classmethod
    def select_test(cls, test_id: str):
        try:
            return cls.tests[test_id]
        except KeyError:
            raise KeyError("Test not found in the TestSelector.")
