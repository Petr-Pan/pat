from typing import List

from flask_wtf import FlaskForm
from wtforms import SubmitField, RadioField
from wtforms.validators import InputRequired

from app.errors.form_error_msg import FormErrorMsg
from app.tests.common import Item


def generate_test_field(item: Item):
    """Generates a RadioField for a given Item."""
    choices = [(response.score, response.text) for response in item.responses]
    label = item.form_prefix + item.statement
    return RadioField(label=label, choices=choices, coerce=int,
                      validators=[InputRequired(message=FormErrorMsg.RadioField.Required)])


def generate_form(items: List[Item]):
    """Creates a form with a dynamic number of fields."""
    class DynamicTestForm(FlaskForm):
        pass

    for item in items:
        field = generate_test_field(item)
        setattr(DynamicTestForm, item.id, field)

    setattr(DynamicTestForm, 'submit', SubmitField('Vyhodnotit test'))

    return DynamicTestForm
