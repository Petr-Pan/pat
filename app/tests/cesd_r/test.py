from app.tests.common import Item, Response, IdGenerator
from app.tests.forms import generate_form


class CESD_R:
    """The Center for Epidemiologic Studies Depression Scale - Revised.
    Flask-friendly implementation."""
    label = "CESD-R"
    client_label = "Škála depresivity"
    client_instructions = "Níže najdete seznam výroků, které popisují, jak jste se mohl/-a v poslední době \
                           cítit nebo chovat. Zaklikněte prosím možnost, která nejlépe vystihuje, jak často \
                           jste se tak cítil/-a za zhruba poslední týden."
    therapist_description = "Revidovaná škála depresivity Centra pro epidemiologické studie (CESD-R)."
    common_responses = [
            Response("Vůbec nebo méně jak 1 den", score=0),
            Response("1-2 dny", score=1),
            Response("3-4 dny", score=2),
            Response("5-7 dní", score=3),
            Response("Skoro každý den po dobu dvou týdnů", score=3)
        ]
    base_item_string = 'item_'
    id_gen = IdGenerator(base_string=base_item_string, starting_number=1)
    items = [
            Item("Neměl jsem chuť k jídlu.", common_responses, id_gen.start_id()),
            Item("Nemohl jsem ze sebe setřást smutnou náladu.", common_responses, id_gen.next_id()),
            Item("Měl jsem problém soustředit se na to, co dělám.", common_responses, id_gen.next_id()),
            Item("Cítil jsem se sklíčený.", common_responses, id_gen.next_id()),
            Item("Měl jsem neklidný spánek.", common_responses, id_gen.next_id()),
            Item("Cítil jsem se smutný.", common_responses, id_gen.next_id()),
            Item("Nedokázal jsem se donutit začít něco dělat.", common_responses, id_gen.next_id()),
            Item("Nic mě netěšilo.", common_responses, id_gen.next_id()),
            Item("Cítil jsem se jako špatný člověk.", common_responses, id_gen.next_id()),
            Item("Přestaly mě zajímat věci, které jsem běžně dělával.", common_responses, id_gen.next_id()),
            Item("Spal jsem výrazně více než obvykle.", common_responses, id_gen.next_id()),
            Item("Měl jsem pocit, že mi běžné věci trvají déle než obvykle.", common_responses, id_gen.next_id()),
            Item("Cítil jsem se neklidný.", common_responses, id_gen.next_id()),
            Item("Přál jsem si, abych byl mrtvý.", common_responses, id_gen.next_id()),
            Item("Chtěl jsem si ublížit.", common_responses, id_gen.next_id()),
            Item("Byl jsem pořád unavený.", common_responses, id_gen.next_id()),
            Item("Neměl jsem se rád.", common_responses, id_gen.next_id()),
            Item("Ztratil jsem spoustu váhy bez toho, abych se o to snažil.", common_responses, id_gen.next_id()),
            Item("Dělalo mi velký problém usnout.", common_responses, id_gen.next_id()),
            Item("Nedokázal jsem se soustředit na důležité věci.", common_responses, id_gen.next_id())
        ]

    form = generate_form(items)
    template = 'tests/radio_test_form.html'
    score_range = [0, 60]

    @staticmethod
    def raw_score(items: dict):
        raw_score = 0
        for item in items.values():
            raw_score += item
        return raw_score
