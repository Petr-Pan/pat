from typing import List


class Response:
    """
    Represents a single response of an item.
    :param text: contains the response text
    :param score: represents the value of the response when the test is scored
    """
    def __init__(self, text: str, score: int):
        self.text = text
        self.score = score


class Item:
    """
    Test item representation.
    :param statement: the main statement or question of the item
    :param responses: the responses to the statement or question
    :param item_id: identification for WTForm processing
    :param form_prefix: extra label prefix for WTForm rendering
    """
    def __init__(self, statement: str, responses: List[Response], item_id: str,
                 form_prefix=None):
        self.statement = statement
        self.responses = responses
        self.id = item_id
        self.form_prefix = form_prefix if form_prefix else item_id.split('_')[1] + '. '


class IdGenerator:
    """Creates enumerated string ids."""
    def __init__(self, base_string: str, starting_number=1):
        self.base_string = base_string
        self.current_number = starting_number
        self.starting_number = starting_number

    def _create_id(self):
        return f"{self.base_string}{self.current_number}"

    def start_id(self):
        self.current_number = self.starting_number
        return self._create_id()

    def next_id(self):
        self.current_number += 1
        return self._create_id()
