"""Provides wrappers for creating input data to graph rendering modules - currently Plotly."""

from app.core.utils import naive_utc_to_local_datetime
from .test_selector import TestSelector


def line(id, tests, y_title='Hrubý skór'):
    """Creates input data for a line chart of provided tests."""
    test_template = TestSelector.select_test(tests[0].test)
    graph = dict(
        id=id,
        data=[
            dict(
                x=[str(naive_utc_to_local_datetime(test.timestamp))
                   for test in tests],
                y=[test.raw_score() for test in tests],
                type='scatter',
                mode='lines+markers'
            ),
        ],
        layout=dict(
            title=test_template.label,
            dragmode='pan',
            xaxis=dict(
                title='Datum',
            ),
            yaxis=dict(
                title=y_title,
                range=test_template.score_range,
                fixedrange=True
            ),
            font=dict(
                family="Segoe UI, monospace",
                size=14,
                # color="#7f7f7f"
            )
        )
    )

    return graph
