from time import time

import jwt
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash

from app import db, login
from app.account.role import Role
from app.model_utils import jwt_encode, jwt_decode, str_uuid4
from app.tests.test_selector import TestSelector
from config import Config, FieldLengths


therapeutic_alliance = db.Table('therapeutic_alliance',
                                db.Column('therapist_id', db.Integer, db.ForeignKey('user.id')),
                                db.Column('client_id', db.Integer, db.ForeignKey('user.id')))


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(FieldLengths.User.Max.Username))
    email = db.Column(db.String(FieldLengths.User.Max.Email), index=True, unique=True)
    password_hash = db.Column(db.String(FieldLengths.User.Max.PasswordHash))
    tests = db.relationship('Test', backref='author', lazy='dynamic')
    thought_records = db.relationship('ThoughtRecord', backref='author', lazy='dynamic')
    role = db.Column(db.Enum(Role, impl=db.String(20)))
    clients = db.relationship('User', secondary=therapeutic_alliance,
                              primaryjoin=(therapeutic_alliance.c.therapist_id == id),
                              secondaryjoin=(therapeutic_alliance.c.client_id == id),
                              backref=db.backref('therapists', lazy='dynamic'), lazy='dynamic')
    public_id = db.Column(db.String,  # Postgres has own UUID data type,
                          # which is not supported by SQLite. In order to keep SQLite
                          # compatibility for development, storage as string is used for now.
                          unique=True, nullable=False, default=str_uuid4)

    def __repr__(self):
        return f'<User {self.username}>'

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def generate_alliance_offer_token(self, client, expire_in=Config.ALLIANCE_OFFER_TOKEN_EXPIRATION_TIME):
        return jwt_encode(
            {'requester': self.public_id,
             'target': client.public_id,
             'exp': time() + expire_in})

    @staticmethod
    def verify_alliance_offer_token(token):
        try:
            alliance = jwt_decode(token)
        except (jwt.DecodeError, jwt.ExpiredSignatureError):
            return None
        return {'therapist': User.query.filter_by(public_id=alliance['requester']).first(),
                'client': User.query.filter_by(public_id=alliance['target']).first()}

    def generate_break_alliance_token(self, therapist, expire_in=Config.BREAK_ALLIANCE_TOKEN_EXPIRATION_TIME):
        return jwt_encode(
            {'requester': self.public_id,
             'target': therapist.public_id,
             'exp': time() + expire_in})

    @staticmethod
    def verify_break_alliance_token(token):
        try:
            alliance = jwt_decode(token)
        except (jwt.DecodeError, jwt.ExpiredSignatureError):
            return None
        return {'client': User.query.filter_by(public_id=alliance['requester']).first(),
                'therapist': User.query.filter_by(public_id=alliance['target']).first()}

    def generate_reset_password_token(self, expire_in=Config.PASSWORD_TOKEN_EXPIRATION_TIME):
        return jwt_encode(
            {'reset_password': self.public_id,
             'exp': time() + expire_in})

    @staticmethod
    def verify_reset_password_token(token):
        try:
            public_id = jwt_decode(token)['reset_password']
        except (jwt.DecodeError, jwt.ExpiredSignatureError):
            return None
        return User.query.filter_by(public_id=public_id).first()

    def generate_self_destruct_token(self, expire_in=Config.SELF_DESTRUCT_TOKEN_EXPIRATION_TIME):
        return jwt_encode(
            {'TNT': self.public_id,
             'exp': time() + expire_in})

    @staticmethod
    def verify_self_destruct_token(token):
        try:
            public_id = jwt_decode(token)['TNT']
        except (jwt.DecodeError, jwt.ExpiredSignatureError):
            return None
        return User.query.filter_by(public_id=public_id).first()

    def add_client(self, user: 'User'):
        if not self.is_in_alliance(user):
            self.clients.append(user)

    def break_alliance(self, user: 'User'):
        if self.role == Role.Therapist and user.role == Role.Client:
            self.clients.remove(user)
        elif self.role == Role.Client and user.role == Role.Therapist:
            self.therapists.remove(user)

    def is_in_alliance(self, user: 'User'):
        if self.role == Role.Therapist:
            return self.clients.filter(user.id == therapeutic_alliance.c.client_id).count() > 0
        elif self.role == Role.Client:
            return self.therapists.filter(user.id == therapeutic_alliance.c.therapist_id).count() > 0


@login.user_loader
def load_user(id):
    return User.query.get(int(id))


class ThoughtRecord(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    timestamp = db.Column(db.DateTime, index=True)
    situation = db.Column(db.String(FieldLengths.ThoughtRecord.Max.Situation))
    automatic_thoughts = db.Column(db.String(FieldLengths.ThoughtRecord.Max.AutomaticThoughts))
    emotions = db.Column(db.String(FieldLengths.ThoughtRecord.Max.Emotions))
    alternative_thoughts = db.Column(db.String(FieldLengths.ThoughtRecord.Max.AlternativeThoughts))
    result = db.Column(db.String(FieldLengths.ThoughtRecord.Max.Result))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))


class Test(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    test = db.Column(db.String(FieldLengths.Test.Max.Test))
    timestamp = db.Column(db.DateTime, index=True)
    item_scores = db.Column(db.PickleType)  # type == dict client-side, type == binary DB-side
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def raw_score(self):
        test = TestSelector.select_test(self.test)
        return test.raw_score(self.item_scores)
