"""Provides functionality that is shared between 'client' and 'therapist' modules."""

from flask import Blueprint

bp = Blueprint('common', __name__)

from app.common import routes
