from flask import render_template

from app.models import ThoughtRecord


def thought_record_browse(client, flask_endpoint, **kwargs):
    """Provides common behaviour between client and therapist view."""
    thought_records = client.thought_records.order_by(ThoughtRecord.timestamp.desc()).all()
    return render_template('common/thought_record_browse.html', title='Přehled záznamů myšlenek', client=client,
                           thought_records=thought_records)
