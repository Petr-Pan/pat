from enum import Enum
from functools import wraps

from flask import flash, redirect, url_for
from flask_login import current_user, login_required


def role_required(*roles):
    def wrapper(func):
        @login_required  # To have a role, user needs to be logged in
        @wraps(func)
        def decorated_view(*args, **kwargs):
            if current_user.role not in roles:
                flash('Pro přístup na tuto stránku nemáte dostatečná oprávnění.')
                return redirect(url_for('core.index'))
            return func(*args, **kwargs)
        return decorated_view
    return wrapper


class Role(Enum):
    Therapist = 'Therapist'
    Client = 'Client'
