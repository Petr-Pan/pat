from flask import current_app, render_template

from app.email.send import send_email
from app.email.utils import czech_expiration_string


def send_password_reset_email(user, forgotten):
    """Sends email to 'user' with a generated password token. Token expiration can be set in application config.
    Parameter 'forgotten' changes email subject and text content to reflect that
    the password was forgotten."""
    subject = "Změna hesla" if not forgotten else "Obnova hesla"
    token = user.generate_reset_password_token()
    token_expiration_string = czech_expiration_string(current_app.config['PASSWORD_TOKEN_EXPIRATION_TIME'])
    send_email(current_app.config['MAIL_SUBJECT_PREFIX'] + subject,
               recipients=[user.email],
               html_body=render_template('email_templates/account/reset_password.html', forgotten=forgotten,
                                         token=token, token_expiration_string=token_expiration_string),
               text_body=render_template('email_templates/account/reset_password.txt', forgotten=forgotten,
                                         token=token, token_expiration_string=token_expiration_string))


def send_client_destroyed_email(target_therapist, destroyed_client):
    """Sends email to therapist that the client has deleted his account."""
    subject = "Oznámení o smazání účtu Vašeho klienta"
    send_email(current_app.config['MAIL_SUBJECT_PREFIX'] + subject,
               recipients=[target_therapist.email],
               html_body=render_template('email_templates/account/client_destroyed.html',
                                         therapist=target_therapist,
                                         client=destroyed_client),
               text_body=render_template('email_templates/account/client_destroyed.txt',
                                         therapist=target_therapist,
                                         client=destroyed_client))


def send_destroyed_therapist_email(target_client, destroyed_therapist):
    """Sends email to client that the therapist has deleted his account."""
    subject = "Oznámení o smazání účtu Vašeho terapeuta"
    send_email(current_app.config['MAIL_SUBJECT_PREFIX'] + subject,
               recipients=[target_client.email],
               html_body=render_template('email_templates/account/therapist_destroyed.html',
                                         client=target_client,
                                         therapist=destroyed_therapist),
               text_body=render_template('email_templates/account/therapist_destroyed.txt',
                                         client=target_client,
                                         therapist=destroyed_therapist))


def send_self_destruct_confirm_email(user):
    """Sends email to user with a generated self destruct token."""
    subject = "Žádost o trvalé smazání účtu"
    token = user.generate_self_destruct_token()
    token_expiration_string = czech_expiration_string(current_app.config['SELF_DESTRUCT_TOKEN_EXPIRATION_TIME'])
    send_email(current_app.config['MAIL_SUBJECT_PREFIX'] + subject,
               recipients=[user.email],
               html_body=render_template('email_templates/account/self_destruct_confirm.html',
                                         user=user, token=token, token_expiration_string=token_expiration_string),
               text_body=render_template('email_templates/account/self_destruct_confirm.txt',
                                         user=user, token=token, token_expiration_string=token_expiration_string))
