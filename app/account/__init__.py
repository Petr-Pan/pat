"""This module covers authentication related functionality, e.g. registering, logging in and out,
restricting access based on user role etc."""

from flask import Blueprint

bp = Blueprint('account', __name__)

from app.account import emails, forms, routes
