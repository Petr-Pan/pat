from flask import flash, redirect, render_template, request, url_for
from flask_login import current_user, login_user, login_required, logout_user

from app import db, limiter
from app.account import bp
from app.account.emails import send_password_reset_email, send_client_destroyed_email,\
    send_destroyed_therapist_email, send_self_destruct_confirm_email
from app.account.forms import ChangePasswordForm, EditProfileForm, LoginForm, RegistrationForm, \
    ResetPasswordRequestForm
from app.account.utils import combine_limits, logged_user_limiter
from app.core.utils import redirect_to_previous
from app.models import User
from app.account.role import Role
from config import Config


@bp.route('/login', methods=['GET', 'POST'])
@limiter.limit(Config.SENSITIVE_ACCOUNT_VIEWS_LIMIT, methods=['POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('core.index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data.lower()).first()
        if user is None or not user.check_password(form.password.data):
            flash("Neplatné heslo nebo email.")
            return redirect(url_for('account.login'))
        login_user(user, remember=form.remember_me.data)
        return redirect_to_previous()
    return render_template('account/login.html', title='Přihlášení', form=form)


@bp.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('core.index'))


@bp.route('/register-client', methods=['GET', 'POST'])
@limiter.limit(Config.REGISTER_NEW_ACCOUNT_LIMIT, methods=['POST'])
def register_client():
    return register(is_therapist=False)


@bp.route('/register-therapist', methods=['GET', 'POST'])
@limiter.limit(Config.REGISTER_NEW_ACCOUNT_LIMIT, methods=['POST'])
def register_therapist():
    return register(is_therapist=True)


@bp.route('/register-path')
def register_path():
    return render_template('account/register_path.html', title='Registrace')


def register(is_therapist):
    title = 'Registrace terapeuta' if is_therapist else 'Registrace klienta'
    if current_user.is_authenticated:
        return redirect(url_for('core.index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data.lower(),
                    role=Role.Therapist if is_therapist else Role.Client)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash("Registrace úspěšná!")
        return redirect(url_for('account.login'))
    return render_template('account/register.html', title=title, form=form)


@bp.route('/edit-profile', methods=['GET', 'POST'])
@limiter.limit(Config.SENSITIVE_ACCOUNT_VIEWS_LIMIT, key_func=logged_user_limiter, methods=['POST'])
@login_required
def edit_profile():
    form = EditProfileForm()
    if form.validate_on_submit():
        current_user.username = form.username.data
        db.session.commit()
        flash("Změna uložena.")
        return redirect(url_for('account.edit_profile'))
    elif request.method == 'GET':
        form.username.data = current_user.username
    therapists = current_user.therapists.all()
    return render_template('account/edit_profile.html', title='Úprava profilu', form=form,
                           therapists=therapists)


@bp.route('/edit-profile/password-change-request')
@limiter.limit(combine_limits([Config.PREVENT_EMAIL_SPAM_VIEW_LIMIT, Config.SENSITIVE_ACCOUNT_VIEWS_LIMIT]), key_func=logged_user_limiter)
@login_required
def password_change_request():
    send_password_reset_email(current_user, forgotten=False)
    flash("Žádost o změnu hesla přijata. Na Vaši emailovou adresu byl odeslán email s dalšími instrukcemi.")
    return redirect(url_for('account.edit_profile'))


@bp.route('/reset-password', methods=['GET', 'POST'])
@limiter.limit(combine_limits([Config.PREVENT_EMAIL_SPAM_VIEW_LIMIT, Config.SENSITIVE_ACCOUNT_VIEWS_LIMIT]), methods=['POST'])
def reset_password():
    if current_user.is_authenticated:
        return redirect(url_for('core.index'))
    form = ResetPasswordRequestForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data.lower()).first()
        if user:
            send_password_reset_email(user, forgotten=True)
        flash(f"Žádost o změnu hesla přijata. Nachází-li se emailová adresa {form.email.data} \
                v naší databázi, bude na ni odeslán email s dalšími instrukcemi.")
        return redirect(url_for('account.login'))
    return render_template('account/reset_password_request.html', title='Obnovení zapomenutého hesla', form=form)


@bp.route('/change-password/<token>', methods=['GET', 'POST'])
@limiter.limit(Config.SENSITIVE_ACCOUNT_VIEWS_LIMIT, methods=['POST'])
def change_password(token):
    user = User.verify_reset_password_token(token)
    if not user:
        flash('Požadavek neúspěšný. Pravděpodobně vypršela platnost odkazu.')
        return redirect(url_for('core.index'))
    if current_user.is_authenticated and current_user != user:  # Covers rare case where user with multiple accounts
        return redirect(url_for('core.index'))        # would be logged on Account1 but would use token for Account2
    form = ChangePasswordForm(user)
    if form.validate_on_submit():
        user.set_password(form.password.data)
        db.session.commit()
        flash("Heslo bylo změněno.")
        if current_user.is_authenticated:
            return redirect(url_for('core.index'))
        else:
            return redirect(url_for('account.login'))
    return render_template('account/change_password.html', title='Změna hesla', form=form)


@bp.route('/self-destruct-request')
@limiter.limit(combine_limits([Config.PREVENT_EMAIL_SPAM_VIEW_LIMIT, Config.SENSITIVE_ACCOUNT_VIEWS_LIMIT]), key_func=logged_user_limiter)
@login_required
def self_destruct_request():
    send_self_destruct_confirm_email(current_user)
    flash("Žádost o trvalé smazání účtu přijata. Na Vaši emailovou adresu byl odeslán email s dalšími instrukcemi.")
    return redirect(url_for('account.edit_profile'))


@bp.route('/self-destruct/<token>')
@login_required
def self_destruct(token):
    user = User.verify_self_destruct_token(token)
    if not user:
        flash('Požadavek neúspěšný. Pravděpodobně vypršela platnost odkazu.')
        return redirect(url_for('core.index'))
    elif current_user != user:
        flash("Odkaz není pro platný pro aktuálně přihlášeného uživatele.")
        return redirect(url_for('core.index'))

    if current_user.role == Role.Client:
        therapists = current_user.therapists.all()
    elif current_user.role == Role.Therapist:
        clients = current_user.clients.all()
    else:
        raise ValueError(f"Role: {current_user.role} has no defined behaviour in self_destruct view")

    db.session.delete(user)
    db.session.commit()

    if current_user.role == Role.Client:
        for therapist in therapists:
            send_client_destroyed_email(therapist, current_user)
    elif current_user.role == Role.Therapist:
        for client in clients:
            send_destroyed_therapist_email(client, current_user)
    else:
        raise ValueError(f"Role: {current_user.role} has no defined behaviour in self_destruct view")
    return render_template('account/self_destroyed.html', title='Účet smazán', user=current_user)
