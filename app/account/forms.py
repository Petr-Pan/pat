from flask_wtf import FlaskForm
from wtforms import BooleanField, PasswordField, StringField, SubmitField
from wtforms.validators import DataRequired, Email, EqualTo, Length, ValidationError

from app.errors.form_error_msg import FormErrorMsg
from app.models import User
from config import FieldLengths


class Common:
    """Class for unifying long common attributes."""
    PasswordValidators = [
            DataRequired(message=FormErrorMsg.Password.Required),
            Length(min=FieldLengths.User.Min.Password, message=FormErrorMsg.Password.MinLength),
            Length(max=FieldLengths.User.Max.Password, message=FormErrorMsg.Password.MaxLength)
        ]

    UsernameValidators = [
            DataRequired(message=FormErrorMsg.Username.Required),
            Length(max=FieldLengths.User.Max.Username, message=FormErrorMsg.Username.MaxLength)
        ]

    @staticmethod
    def validate_password_strength(password_field, username_value, email_value):
        """Checks that password matches neither username nor email."""
        if password_field.data == username_value:
            password_field.errors.append(FormErrorMsg.Password.EqualToUsername)
        elif password_field.data == email_value:
            password_field.errors.append(FormErrorMsg.Password.EqualToEmail)
        else:
            return True
        return False


class LoginForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(message=FormErrorMsg.Email.Required)])
    password = PasswordField('Heslo', validators=[DataRequired(message=FormErrorMsg.Password.Required)])
    remember_me = BooleanField('Uložit přihlášení')
    submit = SubmitField('Přihlásit se')


class RegistrationForm(FlaskForm):
    username = StringField('Uživatelské jméno', validators=Common.UsernameValidators)
    email = StringField('Email', validators=[DataRequired(message=FormErrorMsg.Email.Required),
                                             Email(message=FormErrorMsg.Email.Invalid)])
    password = PasswordField('Heslo', validators=Common.PasswordValidators)
    password2 = PasswordField('Zopakujte heslo', validators=[EqualTo('password', message=FormErrorMsg.Password.NotEqual)])
    submit = SubmitField('Zaregistrovat')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError(FormErrorMsg.Email.Duplicate)

    def validate(self):
        """Custom override of 'validate' method to prevent empty records. Using 'validate_name'
        does not allow validating multiple fields at once."""
        if not super().validate():  # So that other validators are called as well
            return False

        if not Common.validate_password_strength(self.password, self.username.data, self.email.data):
            return False
        else:
            return True


class EditProfileForm(FlaskForm):
    username = StringField('Uživatelské jméno', validators=Common.UsernameValidators)
    submit = SubmitField('Uložit')


class ResetPasswordRequestForm(FlaskForm):
    email = StringField('Zaregistrovaný email', validators=[DataRequired(message=FormErrorMsg.Email.Required),
                                                            Email(message=FormErrorMsg.Email.Invalid)])
    submit = SubmitField('Resetovat heslo')


class ChangePasswordForm(FlaskForm):
    def __init__(self, user):
        super().__init__()
        self.user = user

    password = PasswordField('Nové heslo', validators=Common.PasswordValidators)
    password2 = PasswordField('Zopakujte heslo',
                              validators=[EqualTo('password', message=FormErrorMsg.Password.NotEqual)])
    submit = SubmitField('Změnit heslo')

    def validate(self):
        """Custom override of 'validate' method to prevent empty records. Using 'validate_name'
        does not allow validating multiple fields at once."""
        if not super().validate():  # So that other validators are called as well
            return False

        if not Common.validate_password_strength(self.password, self.user.username, self.user.email):
            return False
        else:
            return True
