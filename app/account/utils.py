from typing import List

from flask_limiter.util import get_remote_address
from flask_login import current_user


def combine_limits(limit_strings: List[str]):
    """Flask-Limiter allows to combine limits provided as a string with a tab separator.
    This is a wrapper function for easier management in case Flask-Limiter changes this behaviour."""
    output = ''
    for limit_string in limit_strings:
        output += limit_string + ';'
    return output[:-1]


def common_endpoints_limiter():
    """Returns appropriate key function depending on whether user is authenticated."""
    if current_user.is_authenticated:
        return logged_user_limiter()
    else:
        return get_remote_address


def logged_user_limiter():
    """Key for differentiating logged in users by web usage limiters."""
    return current_user.public_id
