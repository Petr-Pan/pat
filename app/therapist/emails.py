from flask import current_app, render_template

from app.email.send import send_email
from app.email.utils import czech_expiration_string


def send_alliance_end_email(target_client, requester_therapist):
    """Sends email to the client with whom the therapist has ended an alliance."""
    subject = "Zpráva o ukončení terapeutického vztahu"
    send_email(current_app.config['MAIL_SUBJECT_PREFIX'] + subject,
               recipients=[target_client.email],
               html_body=render_template('email_templates/therapist/alliance_end_announcement.html',
                                         client=target_client,
                                         therapist=requester_therapist),
               text_body=render_template('email_templates/therapist/alliance_end_announcement.txt',
                                         client=target_client,
                                         therapist=requester_therapist))


def send_alliance_offer_email(target_client, requester_therapist):
    """Sends an alliance offer email to the target client from requester therapist."""
    subject = "Nabídka vytvoření terapeutického vztahu"
    token = requester_therapist.generate_alliance_offer_token(target_client)
    token_expiration_string = czech_expiration_string(
        current_app.config['ALLIANCE_OFFER_TOKEN_EXPIRATION_TIME'])
    send_email(current_app.config['MAIL_SUBJECT_PREFIX'] + subject,
               recipients=[target_client.email],
               html_body=render_template('email_templates/therapist/alliance_offer.html',
                                         client=target_client,
                                         therapist=requester_therapist,
                                         token=token,
                                         token_expiration_string=token_expiration_string),
               text_body=render_template('email_templates/therapist/alliance_offer.txt',
                                         client=target_client,
                                         therapist=requester_therapist,
                                         token=token,
                                         token_expiration_string=token_expiration_string))
