from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired, Email

from app.errors.form_error_msg import FormErrorMsg


class AddClientForm(FlaskForm):
    email = StringField('Email, pod kterým se klient registroval',
                        validators=[DataRequired(message=FormErrorMsg.Email.Required),
                                    Email(message=FormErrorMsg.Email.Invalid)])
    submit = SubmitField('Přidat klienta')
