from flask import flash, Markup, redirect, render_template, url_for
from flask_login import current_user

from app import common, db, limiter
from app.account.role import Role, role_required
from app.account.utils import logged_user_limiter
from app.therapist import bp
from app.therapist.emails import send_alliance_end_email, send_alliance_offer_email
from app.therapist.forms import AddClientForm
from app.models import Test, ThoughtRecord, User
from app.tests.cesd_r import CESD_R
from app.tests import graphs as test_graphs
from config import Config


@bp.route('client/<public_id>/thought-record-browse/')
@role_required(Role.Therapist)
def thought_record_browse(public_id):
    client = User.query.filter_by(public_id=public_id).first_or_404()
    return common.routes.thought_record_browse(client, 'therapist.thought_record_browse',
                                               public_id=public_id)


@bp.route('/list-of-clients')
@role_required(Role.Therapist)
def list_of_clients():  # TODO: cells with links to clients' lists of tests and TRecords could show timestamp of last test/TRecord written
    return render_template('therapist/list_of_clients.html', user=current_user, clients=current_user.clients.all())


@bp.route('/add-client', methods=['GET', 'POST'])
@limiter.limit(Config.ADD_CLIENT_VIEW_LIMIT, key_func=logged_user_limiter, methods=['POST'])
@role_required(Role.Therapist)
def add_client():
    form = AddClientForm()
    if form.validate_on_submit():
        form_email = form.email.data.lower()
        if current_user.email == form_email:
            flash("Nelze sledovat sebe sama.")
            return redirect(url_for('therapist.add_client'))
        client = User.query.filter(User.email == form_email) \
                           .filter(User.role == Role.Client).first()
        if client:
            if client.is_in_alliance(current_user):
                flash(Markup(f"Klient na emailu {form_email} je s Vámi již propojen. <br><br> \
                               Klienta lze nalézt vyhledáním dle emailu v tabulce níže nebo na \
                               <a href='{url_for('therapist.client_overview', public_id=client.public_id)}'> \
                               tomto odkazu</a>."))
                return redirect(url_for('therapist.list_of_clients'))
            send_alliance_offer_email(client, current_user)
        flash(f"Zpracováváme Vaši žádost o propojení s klientem na emailu: {form_email}. \
                Nachází-li se v naší databázi, bude mu odeslána pozvánka. O jejím přijetí budete notifikováni \
                emailem.")
        return redirect(url_for('therapist.list_of_clients'))
    return render_template('therapist/add_client.html', user=current_user, form=form)


@bp.route('/remove-client/<public_id>')
@role_required(Role.Therapist)
def remove_client(public_id):
    client = User.query.filter_by(public_id=public_id).first_or_404()
    if current_user.is_in_alliance(client):
        current_user.break_alliance(client)
        db.session.commit()
        send_alliance_end_email(client, current_user)
        flash(f"Uživatel {client.email} odebrán.")
    return redirect(url_for('therapist.list_of_clients'))


@bp.route('/client/<public_id>')
@role_required(Role.Therapist)
def client_overview(public_id):
    client = User.query.filter_by(public_id=public_id).first_or_404()
    tests = client.tests.filter_by(test=CESD_R.label).order_by(Test.timestamp.desc()).all()
    thought_records = client.thought_records.order_by(ThoughtRecord.timestamp.desc()).all()
    graphs = [test_graphs.line(CESD_R.label, tests)] if tests else None
    return render_template('therapist/client_overview.html', client=client, thought_records=thought_records,
                           tests=tests, graphs=graphs)


@bp.route('/client/<public_id>/test-browse')
@role_required(Role.Therapist)
def test_browse(public_id):
    client = User.query.filter_by(public_id=public_id).first_or_404()
    tests = client.tests.order_by(Test.timestamp.desc()).all()
    return render_template('therapist/test_browse.html', client=client, tests=tests)


@bp.route('/manual')
@role_required(Role.Therapist)
def manual():
    return render_template('therapist/manual.html', title='Návod k užívání PAT')
