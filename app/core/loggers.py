import logging
from logging.handlers import RotatingFileHandler, SMTPHandler
from pathlib import Path


def init_email_logging(app):
    auth = None
    if app.config['MAIL_USERNAME'] and app.config['MAIL_PASSWORD']:
        auth = (app.config['MAIL_USERNAME'], app.config['MAIL_PASSWORD'])
    secure = None
    if app.config['MAIL_USE_TLS']:
        secure = ()  # Empty tuple means that TLS without a certificate and key will be used
    mail_handler = SMTPHandler(
        mailhost=(app.config['MAIL_SERVER'], app.config['MAIL_PORT']),
        fromaddr=app.config['ADMINS'][0],
        toaddrs=app.config['ADMINS'], subject='PAT Error',
        credentials=auth, secure=secure)
    mail_handler.setLevel(logging.ERROR)
    app.logger.addHandler(mail_handler)


def init_file_logging(app):
    if not Path.exists(Path('logs')):
        Path.mkdir(Path('logs'))
    file_handler = RotatingFileHandler('logs/PAT.log', maxBytes=10240, backupCount=10, encoding='utf-8')
    file_handler.setFormatter(logging.Formatter(
        '%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]'))
    file_handler.setLevel(logging.INFO)
    app.logger.addHandler(file_handler)
