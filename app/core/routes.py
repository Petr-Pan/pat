from flask import flash, redirect, render_template, url_for

from app import limiter
from app.account.utils import common_endpoints_limiter
from app.core import bp
from app.core.emails import send_user_support_email
from app.core.forms import ContactUserSupportForm
from config import Config


@bp.route('/about')
def about():
    return render_template('core/about.html', title='O PAT')


@bp.route('/client-introduction')
def client_intro():
    return render_template('core/client_intro.html', title='Úvod pro klienty')


@bp.route('/therapist-introduction')
def therapist_intro():
    return render_template('core/therapist_intro.html', title='Úvod pro terapeuty')


@bp.route('/contact-user-support', methods=['GET', 'POST'])
@limiter.limit(Config.CONTACT_USER_SUPPORT_VIEW_LIMIT, key_func=common_endpoints_limiter, methods=['POST'])
def contact_user_support():
    form = ContactUserSupportForm()
    if form.validate_on_submit():
        send_user_support_email(message=form.message.data,
                                user_subject=form.subject.data,
                                reply_email=form.email.data)
        flash("Zpráva odeslána.")
        return redirect(url_for('core.index'))
    return render_template('core/contact_user_support.html', title='Kontaktování uživatelské podpory',
                           form=form)


@bp.route('/finding-help')
def finding_help():
    return render_template('core/finding_help.html', title='Vyhledání pomoci')


@bp.route('/')
@bp.route('/index')
def index():
    return render_template('core/index.html', title='Úvodní stránka')


@bp.route('/licence')
def licence():
    return render_template('core/licence.html', title='Licence')


@bp.route('/privacy')
def privacy():
    return render_template('core/privacy.html', title='Soukromí')


@bp.route('/psychology-information')
def psychoeducational_materials():
    return render_template('core/psychoeducational_materials.html', title='Informace')


@bp.route('/psychological-background')
def psychological_background():
    return render_template('core/psychological_background.html', title='Informace pro terapeuty')
