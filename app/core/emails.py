from flask import current_app, render_template

from app.email.send import send_email


def send_user_support_email(message, user_subject=None, reply_email=None):
    """Sends email to the configured user support email of the app."""
    _subject = user_subject if user_subject else "Zpráva bez vyplněného předmětu"
    subject = "[Podpora] - " + _subject
    send_email(current_app.config['MAIL_SUBJECT_PREFIX'] + subject,
               recipients=[current_app.config['USER_SUPPORT']],
               html_body=render_template('email_templates/core/user_support_form_email.html',
                                         subject=subject,
                                         message=message,
                                         reply_email=reply_email),
               text_body=render_template('email_templates/core/user_support_form_email.txt',
                                         subject=subject,
                                         message=message,
                                         reply_email=reply_email))
