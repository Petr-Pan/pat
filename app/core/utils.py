import pytz
from datetime import datetime, timezone

from flask import current_app, redirect, request, url_for
from werkzeug.urls import url_parse


def current_local_datetime():
    return datetime.now(pytz.timezone(current_app.config['DEFAULT_TIMEZONE']))


def pagination_urls(flask_endpoint, paginate_object, **kwargs):
    """Generates URLs for previous and next pages of a content endpoint,
    using paginate object from Flask-SQLAlchemy."""
    prev_url = url_for(flask_endpoint, page=paginate_object.prev_num, **kwargs) if paginate_object.has_prev else None
    next_url = url_for(flask_endpoint, page=paginate_object.next_num, **kwargs) if paginate_object.has_next else None
    return prev_url, next_url


def redirect_to_previous():
    next_page = request.args.get('next')
    if not next_page or url_parse(
            next_page).netloc != '':  # Second condition is a security measure - it prevents redirecting to a foreign website
        next_page = url_for('core.index')
    return redirect(next_page)


def naive_utc_to_local_datetime(naive_datetime):
    return naive_datetime.replace(tzinfo=timezone.utc).astimezone(pytz.timezone(current_app.config['DEFAULT_TIMEZONE']))
