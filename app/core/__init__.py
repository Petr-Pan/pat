"""This module covers the base of the app and general functionality that is shared between multiple modules."""

from flask import Blueprint

bp = Blueprint('core', __name__)

from app.core import routes, utils
