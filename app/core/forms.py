from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, TextAreaField
from wtforms.validators import DataRequired, Email, Length, Optional

from config import FieldLengths
from app.errors.form_error_msg import FormErrorMsg


class ContactUserSupportForm(FlaskForm):
    email = StringField('Email pro obdržení odpovědi (nepovinný)',
                        validators=[Email(message=FormErrorMsg.Email.Invalid),
                                    Optional()])
    subject = StringField('Předmět zprávy', validators=[Length(max=FieldLengths.Message.Max.UserSupportMailSubject,
                                                               message=FormErrorMsg.UserSupportMailSubject.MaxLength)])
    message = TextAreaField('Zpráva',
                            validators=[DataRequired(message=FormErrorMsg.UserSupportMail.Required),
                                        Length(max=FieldLengths.Message.Max.UserSupportMail,
                                               message=FormErrorMsg.UserSupportMail.MaxLength)])
    submit = SubmitField('Odeslat zprávu')
