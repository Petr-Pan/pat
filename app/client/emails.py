from flask import current_app, render_template

from app.email.send import send_email
from app.email.utils import czech_expiration_string


def send_alliance_accepted_email(requester_therapist, target_client):
    """Sends email to therapist that client has accepted the offered alliance."""
    subject = "Oznámení o přijetí terapeutického vztahu"
    send_email(current_app.config['MAIL_SUBJECT_PREFIX'] + subject,
               recipients=[requester_therapist.email],
               html_body=render_template('email_templates/client/alliance_accepted.html',
                                         therapist=requester_therapist,
                                         client=target_client),
               text_body=render_template('email_templates/client/alliance_accepted.txt',
                                         therapist=requester_therapist,
                                         client=target_client))


def send_break_alliance_confirmation_email(requester_client, target_therapist):
    """Sends email to client for confirmation of alliance breaking.
    Token expiration can be set in application config."""
    subject = "Ukončení terapeutického vztahu"
    token = requester_client.generate_break_alliance_token(target_therapist)
    token_expiration_string = czech_expiration_string(
        current_app.config['BREAK_ALLIANCE_TOKEN_EXPIRATION_TIME'])
    send_email(current_app.config['MAIL_SUBJECT_PREFIX'] + subject,
               recipients=[requester_client.email],
               html_body=render_template('email_templates/client/break_alliance_confirmation.html',
                                         client=requester_client,
                                         therapist=target_therapist,
                                         token=token,
                                         token_expiration_string=token_expiration_string),
               text_body=render_template('email_templates/client/break_alliance_confirmation.txt',
                                         client=requester_client,
                                         therapist=target_therapist,
                                         token=token,
                                         token_expiration_string=token_expiration_string))


def send_farewell_email(target_therapist, requester_client, message):
    """Sends email to the therapist with whom the client has ended an alliance.
    Enclosed is client's message to the therapist, if the client sent any."""
    subject = "Zpráva o ukončení terapeutického vztahu"
    send_email(current_app.config['MAIL_SUBJECT_PREFIX'] + subject,
               recipients=[target_therapist.email],
               html_body=render_template('email_templates/client/farewell_message.html',
                                         therapist=target_therapist,
                                         client=requester_client,
                                         message=message),
               text_body=render_template('email_templates/client/farewell_message.txt',
                                         therapist=target_therapist,
                                         client=requester_client,
                                         message=message))
