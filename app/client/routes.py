from datetime import datetime, timezone
import pytz

from flask import current_app, flash, Markup, redirect, render_template, request, url_for
from flask_login import current_user

from app import common, db, limiter
from app.account.role import Role, role_required
from app.account.utils import logged_user_limiter
from app.core.utils import current_local_datetime, naive_utc_to_local_datetime, redirect_to_previous
from app.client import bp
from app.client.emails import send_alliance_accepted_email, send_break_alliance_confirmation_email, send_farewell_email
from app.client.forms import FarewellForm, ThoughtRecordForm
from app.models import Test, ThoughtRecord, User
from app.tests import graphs as test_graphs
from app.tests.cesd_r import CESD_R
from app.tests.test_selector import TestSelector
from config import Config


@bp.route('/accept-alliance/<token>')
@role_required(Role.Client)
def accept_alliance(token):
    alliance_members = User.verify_alliance_offer_token(token)
    if not alliance_members:
        flash("Požadavek neúspěšný. Pravděpodobně vypršela platnost odkazu.")
        return redirect(url_for('core.index'))

    client = alliance_members['client']
    if client != current_user:
        flash("Propojení nelze provést - použitý odkaz patří jinému uživateli.")
        return redirect(url_for('core.index'))

    therapist = alliance_members['therapist']
    if client.is_in_alliance(therapist):
        flash("Tento terapeut je s Vámi již propojen.")
        return redirect(url_for('core.index'))
    else:
        therapist.add_client(client)
        db.session.commit()
        send_alliance_accepted_email(therapist, client)
        return render_template('client/alliance_created.html', title='Terapeutická aliance vytvořena',
                               client=client, therapist=therapist)


@bp.route('/break-alliance-request/<public_id>')
@limiter.limit(Config.PREVENT_EMAIL_SPAM_VIEW_LIMIT, key_func=logged_user_limiter)
@role_required(Role.Client)
def break_alliance_request(public_id):
    therapist = User.query.filter_by(public_id=public_id).first_or_404()
    send_break_alliance_confirmation_email(current_user, therapist)
    flash("Žádost o ukončení terapeutického vztahu přijata. \
           Na Vaši emailovou adresu byl odeslán email s dalšími instrukcemi.")
    return redirect(url_for('account.edit_profile'))


@bp.route('/break-alliance/<token>/<send_message>', methods=['GET', 'POST'])
@role_required(Role.Client)
def break_alliance(token, send_message):
    alliance_members = User.verify_break_alliance_token(token)
    if not alliance_members:
        flash("Požadavek neúspěšný. Pravděpodobně vypršela platnost odkazu, \
               nebo již byl tento terapeut od Vás odpojen.")
        return redirect(url_for('core.index'))
    client = alliance_members['client']
    if client != current_user:
        flash("Zrušení nelze provést - použitý odkaz patří jinému uživateli.")
        return redirect(url_for('core.index'))
    therapist = alliance_members['therapist']

    if not client.is_in_alliance(therapist):
        flash("Tento teraput již od Vás byl odpojen. \
               Pokud je pro Vás toto překvapením, zkontrolujte svou emailovou schránku. \
               Je možné, že Váš terapeut zrušil propojení před Vámi.")
        return redirect(url_for('core.index'))

    if send_message == 'False':
        current_user.break_alliance(therapist)
        db.session.commit()
        send_farewell_email(therapist, client, message=None)
        return render_template('client/alliance_end.html', title='Zrušení propojení',
                               client=client, therapist=therapist)
    else:
        form = FarewellForm()
        if form.validate_on_submit():
            if client.is_in_alliance(therapist):
                current_user.break_alliance(therapist)
                db.session.commit()
                send_farewell_email(therapist, client, form.message.data)
                flash("Zpráva odeslána. Děkujeme za zpětnou vazbu.")
                return render_template('client/alliance_end.html', title='Zrušení propojení',
                                       client=client, therapist=therapist)
            else:
                flash("Tento teraput již od Vás byl odpojen. Zprávu nelze odeslat. \
                       Pokud je pro Vás toto překvapením, zkontrolujte svou emailovou schránku. \
                       Je možné, že Váš terapeut zrušil propojení před Vámi.")
                flash(Markup(f'Zde je kopie Vaší zprávy, pokud byste ji chtěl/-a poslat emailem: <br> \
                               """<br>{form.message.data}<br>"""'))
                return redirect(url_for('core.index'))
        return render_template('client/alliance_farewell.html', title='Zrušení propojení',
                               client=client, therapist=therapist, token=token, form=form)


@bp.route('/client-manual')
@role_required(Role.Client)
def manual():
    return render_template('client/manual.html', title='Návod k užívání aplikace')


@bp.route('/thought-record-overview')
@role_required(Role.Client)
def thought_record_overview():
    thought_records = current_user.thought_records.order_by(ThoughtRecord.timestamp.desc()) \
        .paginate(1, current_app.config['THOUGHT_RECORDS_PER_PAGE_OVERVIEW'], False)
    return render_template('client/thought_record_overview.html', title='Záznamy myšlenek',
                           thought_records=thought_records.items)


@bp.route('/new-thought-record', methods=['GET', 'POST'])
@limiter.limit(Config.UNCOMMON_POST_REQUEST_VIEWS_LIMIT, key_func=logged_user_limiter, methods=['POST'])
@role_required(Role.Client)
def new_thought_record():
    form = ThoughtRecordForm()
    if form.validate_on_submit():
        date = form.date.data
        time = form.time.data
        local_tz = pytz.timezone(current_app.config['DEFAULT_TIMEZONE'])
        timestamp = local_tz.localize(datetime.combine(date, time)).astimezone(timezone.utc)
        thought_record = ThoughtRecord(timestamp=timestamp,
                                       situation=form.situation.data,
                                       automatic_thoughts=form.automatic_thoughts.data,
                                       emotions=form.emotions.data,
                                       alternative_thoughts=form.alternative_thoughts.data,
                                       result=form.result.data,
                                       user_id=current_user.id)
        db.session.add(thought_record)
        db.session.commit()
        flash("Myšlenka zapsána.")
        return redirect(url_for('client.thought_record_overview'))
    elif request.method == 'GET':
        current_datetime = current_local_datetime()
        form.time.data = current_datetime
        form.date.data = current_datetime
    return render_template('client/new_thought_record.html', title='Nový záznam', form=form)


@bp.route('/edit-thought-record/<id>', methods=['GET', 'POST'])
@limiter.limit(Config.UNCOMMON_POST_REQUEST_VIEWS_LIMIT, key_func=logged_user_limiter, methods=['POST'])
@role_required(Role.Client)
def edit_thought_record(id):
    thought_record = ThoughtRecord.query.filter_by(user_id=current_user.id, id=id).first_or_404()
    form = ThoughtRecordForm()
    if form.validate_on_submit():
        date = form.date.data
        time = form.time.data
        local_tz = pytz.timezone(current_app.config['DEFAULT_TIMEZONE'])
        timestamp = local_tz.localize(datetime.combine(date, time)).astimezone(timezone.utc)
        thought_record.timestamp = timestamp
        thought_record.situation = form.situation.data
        thought_record.automatic_thoughts = form.automatic_thoughts.data
        thought_record.emotions = form.emotions.data
        thought_record.alternative_thoughts = form.alternative_thoughts.data
        thought_record.result = form.result.data
        db.session.commit()
        flash("Úspěšně upraveno.")
        return redirect_to_previous()
    elif request.method == 'GET':
        timestamp = naive_utc_to_local_datetime(thought_record.timestamp)
        form.time.data = timestamp
        form.date.data = timestamp
        form.situation.data = thought_record.situation
        form.automatic_thoughts.data = thought_record.automatic_thoughts
        form.emotions.data = thought_record.emotions
        form.alternative_thoughts.data = thought_record.alternative_thoughts
        form.result.data = thought_record.result
    return render_template('client/new_thought_record.html', title='Upravit záznam', form=form)


@bp.route('/delete-thought-record/<id>')
@limiter.limit(Config.UNCOMMON_POST_REQUEST_VIEWS_LIMIT, key_func=logged_user_limiter)
@role_required(Role.Client)
def delete_thought_record(id):
    thought_record = ThoughtRecord.query.filter_by(user_id=current_user.id, id=id).first_or_404()
    db.session.delete(thought_record)
    db.session.commit()
    flash("Záznam úspěšně smazán.")
    return redirect_to_previous()


@bp.route('/thought-record-browse/')
@role_required(Role.Client)
def thought_record_browse():
    client = current_user
    return common.routes.thought_record_browse(client, 'client.thought_record_browse')


@bp.route('/test-overview/')
@role_required(Role.Client)
def test_overview():
    tests = current_user.tests.order_by(Test.timestamp.desc()).all()
    graphs = [test_graphs.line(CESD_R.client_label, tests)] if tests else None
    return render_template('client/test_overview.html', title='Dotazníky', tests=tests,
                           graphs=graphs)


@bp.route('/take-test/<test_id>', methods=['GET', 'POST'])
@limiter.limit(Config.UNCOMMON_POST_REQUEST_VIEWS_LIMIT, key_func=logged_user_limiter, methods=['POST'])
@role_required(Role.Client)
def take_test(test_id):
    test = TestSelector.select_test(test_id)
    form = test.form()
    if form.validate_on_submit():
        timestamp = datetime.utcnow()
        item_scores = {item_id: score for item_id, score in form.data.items()
                       if test.base_item_string in item_id}
        test = Test(timestamp=timestamp,
                    test=test_id,
                    item_scores=item_scores,
                    user_id=current_user.id)
        db.session.add(test)
        db.session.commit()
        flash("Test uložen a vyhodnocen.")
        return redirect(url_for('client.test_overview'))
    return render_template(test.template, title=test.label, form=form,
                           test=test)


@bp.route('/delete-test/<id>')
@limiter.limit(Config.UNCOMMON_POST_REQUEST_VIEWS_LIMIT, key_func=logged_user_limiter)
@role_required(Role.Client)
def delete_test(id):
    test = current_user.tests.filter_by(id=id).first_or_404()
    db.session.delete(test)
    db.session.commit()
    flash("Test úspěšně smazán.")
    return redirect(url_for('client.test_overview'))
