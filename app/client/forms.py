from flask_wtf import FlaskForm
from wtforms import SubmitField, TextAreaField
from wtforms.validators import DataRequired, Length
from wtforms.fields.html5 import DateField, TimeField

from app.errors.form_error_msg import FormErrorMsg
from config import FieldLengths


class FarewellForm(FlaskForm):
    message = TextAreaField('Zpráva pro terapeuta', validators=[DataRequired(message=FormErrorMsg.Farewell.Required),
                                                                Length(max=FieldLengths.Message.Max.Farewell,
                                                                       message=FormErrorMsg.Farewell.MaxLength)])
    submit = SubmitField('Odeslat')


class ThoughtRecordForm(FlaskForm):
    date = DateField('Datum', validators=[DataRequired(message=FormErrorMsg.Date.Required)])
    time = TimeField('Čas', validators=[DataRequired(message=FormErrorMsg.Time.Required)])
    situation = TextAreaField('Situace',
                              validators=[Length(max=FieldLengths.ThoughtRecord.Max.Situation,
                                                 message=FormErrorMsg.max_length_error(
                                                     FieldLengths.ThoughtRecord.Max.Situation))])
    automatic_thoughts = TextAreaField('Automatické myšlenky',
                                       validators=[Length(max=FieldLengths.ThoughtRecord.Max.AutomaticThoughts,
                                                          message=FormErrorMsg.max_length_error(
                                                              FieldLengths.ThoughtRecord.Max.AutomaticThoughts))])
    emotions = TextAreaField('Emoce',
                             validators=[Length(max=FieldLengths.ThoughtRecord.Max.Emotions,
                                                message=FormErrorMsg.max_length_error(
                                                    FieldLengths.ThoughtRecord.Max.Emotions))])
    alternative_thoughts = TextAreaField('Prospěšná odpověď',
                                         validators=[Length(max=FieldLengths.ThoughtRecord.Max.AlternativeThoughts,
                                                            message=FormErrorMsg.max_length_error(
                                                                FieldLengths.ThoughtRecord.Max.AlternativeThoughts))])
    result = TextAreaField('Výsledek',
                           validators=[Length(max=FieldLengths.ThoughtRecord.Max.Result,
                                              message=FormErrorMsg.max_length_error(
                                                  FieldLengths.ThoughtRecord.Max.Result))])
    submit = SubmitField('Uložit myšlenku')

    def validate(self):
        """Custom override of 'validate' method to prevent empty records. Using 'validate_name'
        does not allow validating multiple fields at once."""
        if not super().validate():  # So that other validators are called as well
            return False

        if not self.situation.data \
                and not self.automatic_thoughts.data \
                and not self.emotions.data \
                and not self.alternative_thoughts.data \
                and not self.result.data:
            self.result.errors.append(FormErrorMsg.ThoughtRecord.Empty)
            return False
        else:
            return True
