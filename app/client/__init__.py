"""This module provides functionality for treated clients."""

from flask import Blueprint

bp = Blueprint('client', __name__)

from app.client import forms, routes
