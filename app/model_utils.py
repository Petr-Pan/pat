import jwt
from uuid import uuid4

from flask import current_app


def jwt_encode(data: dict):
    """Wrapper for jwt encoding."""
    return jwt.encode(data, current_app.config['SECRET_KEY'], algorithm='HS256').decode('utf-8')


def jwt_decode(token: str):
    """Wrapper for jwt decoding."""
    return jwt.decode(token, current_app.config['SECRET_KEY'], algorithms=['HS256'])


def str_uuid4():
    return str(uuid4())
