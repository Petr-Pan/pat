# PAT

A web application for computer assisted therapy, originally developed for bachelor thesis.

The application is meant to serve as a supplementary method 
for treating depressive disorders with cognitive-behavioral therapy.

## Installation
1. Clone master branch.

There are two build options.

### Manual build
1. Configure the app - check .flaskenv and config.py files and set them to your liking.
1. Install requirements with `pip install -r requirements.txt`.
1. Serve the 'pat.py' directly to your WSGI server.

    
### Automated Docker build
There are three environments prepared - development, staging and production.

#### Common for all environments
1. Check config.py for configuration variables and set appropriately.

#### Development
The app is run locally via the flask WSGI server, with SQLite database. Meant for quick testing of changes.

1. Create and activate python virtual environment (`python -m venv venv`)
1. Install requirements with `pip install -r requirements.txt`
1. Configure '.flaskenv' file to match your environment
1. Run `flask db upgrade` to initialize SQLite database for this environment
1. Start the app with `flask run`

On some systems, `python -m flask run` may be needed instead.

#### Staging and production
This way three deploy-ready docker containers are used. Nginx is used as the web server, gunicorn as WSGI and
postgres as database. 

Docker needs to be installed on your host system.

1. Copy your SSL certificate into '/nginx/certs'.

1. Edit .env.stage and .env.stage_db files to match your environment 
    (.env.prod and .env.prod_db for production environment).
    Double curly brackets indicate values that need to be replaced and should be removed afterwards.
    * DATABASE_URL has the format of `postgresql://{{POSTGRES_USER}}:{{POSTGRES_PASSWORD}}@db:5432/{{POSTGRES_DB}}`.
    Replace double curly brackets and their content with corresponding values that you set inside .env db files 
    (.env.stage_db for stage and .env.prod_db for production)

1. Inside docker-compose.yml, in services.nginx.ports set exposed ports if needed.
    The exposed port on the host is the number before the colon on each line.
    First line configures http port, second one https.
    
1. Build the app with:
    * For staging: `docker-compose -f docker-compose.yml -f docker-compose.stage.yml build`
    * For production: `docker-compose -f docker-compose.yml -f docker-compose.prod.yml build`

1. Run with:
    * For staging: `docker-compose -f docker-compose.yml -f docker-compose.stage.yml up -d`
    * For production: `docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d`
    
The app can then be stopped with `docker-compose stop`.
