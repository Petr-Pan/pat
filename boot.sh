#!/bin/sh
source venv/bin/activate
while true; do
    flask db upgrade
    if [[ "$?" == "0" ]]; then
        break
    fi
    echo Database upgrade failed, retrying in 5 seconds...
    sleep 5
done
exec "$@"
