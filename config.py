import datetime
import os
from pathlib import Path


basedir = Path()


class Config(object):
    """
    Configuration class for the Flask app instance.
    Covers general app settings.
    """
    # Core
    SECRET_KEY = os.environ.get('SECRET_KEY') or "First in, last out."
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or 'sqlite:///' + str(basedir.resolve()) + '/app.db'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    # Mail
    MAIL_SERVER = os.environ.get('MAIL_SERVER')
    MAIL_PORT = int(os.environ.get('MAIL_PORT') or 25)
    MAIL_USE_TLS = os.environ.get('MAIL_USE_TLS') is not None
    MAIL_USERNAME = os.environ.get('MAIL_USERNAME')
    MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')
    ADMINS = ['{{ email of admin }}']  # Receives critical error logs of the application
    MAIL_DEFAULT_SENDER = ('PAT-info', ADMINS[0])
    MAIL_SUBJECT_PREFIX = "PAT - "
    USER_SUPPORT = ADMINS[0]  # Receives emails for user support
    # Generics
    BOOTSTRAP_BTN_STYLE = 'primary'
    BOOTSTRAP_SERVE_LOCAL = True
    DEFAULT_TIMEZONE = 'Europe/Prague'
    # View limits
    ADD_CLIENT_VIEW_LIMIT = '25/day;1/second'
    CONTACT_USER_SUPPORT_VIEW_LIMIT = '20/day'
    PREVENT_EMAIL_SPAM_VIEW_LIMIT = '10/day'
    REGISTER_NEW_ACCOUNT_LIMIT = '5/hour'
    SENSITIVE_ACCOUNT_VIEWS_LIMIT = '5/minute'
    UNCOMMON_POST_REQUEST_VIEWS_LIMIT = '1000/day;1/second'
    # App specific
    # # Token expiration times - check email templates if expiration_time values are set higher than weeks
    ALLIANCE_OFFER_TOKEN_EXPIRATION_TIME = int(datetime.timedelta(weeks=1).total_seconds())
    BREAK_ALLIANCE_TOKEN_EXPIRATION_TIME = int(datetime.timedelta(weeks=1).total_seconds())
    PASSWORD_TOKEN_EXPIRATION_TIME = int(datetime.timedelta(minutes=15).total_seconds())
    SELF_DESTRUCT_TOKEN_EXPIRATION_TIME = int(datetime.timedelta(minutes=15).total_seconds())
    # # Views
    THOUGHT_RECORDS_PER_PAGE_OVERVIEW = 3


class FieldLengths:
    """
    Configuration class for field lengths of public app objects.
    Changing value will update everything related to the field - it's
    database model, form validation in views, validation errors etc.

    Database needs to be migrated afterwards, unless the value
    did not concern database model (e.g. FieldLengths.User.Min.Password).
    """

    class ThoughtRecord:
        class Max:
            Situation = 1000
            AutomaticThoughts = 1000
            Emotions = 500
            AlternativeThoughts = 1000
            Result = 1000

    class User:
        class Max:
            Username = 64
            Email = 128
            Password = 128  # Maximum that user can use during registration
            PasswordHash = 128  # Maximum for database model specification

        class Min:
            Password = 6

    class Test:
        class Max:
            Test = 50

    class Message:
        class Max:
            Farewell = 10000
            UserSupportMail = 10000
            UserSupportMailSubject = 80
