Bootstrap-Flask==1.3.1
flask==1.1.1
Flask-Limiter==1.2.1
Flask-Login==0.4.1
Flask-Mail==0.9.1
Flask-Migrate==2.5.2
Flask-Moment==0.9.0
Flask-SQLAlchemy==2.4.0
Flask-WTF==0.14.3
plotly==4.6.0
PyJWT==1.7.1
pytest==5.3.2
python-dotenv==0.10.3
pytz==2019.3

email-validator==1.0.5  # Temporary fix of WTForms dependency, until they release a new version
