import pytest
import jwt

from app import create_app, db
from app.account.role import Role
from app.models import load_user, User
from config import Config


class UnitTestConfig(Config):
    SQLALCHEMY_DATABASE_URI = 'sqlite://'
    TESTING = True


@pytest.fixture()
def db_setup():
    app = create_app(UnitTestConfig)
    with app.app_context():
        db.create_all()
        yield db
        db.session.remove()
        db.drop_all()


@pytest.fixture()
def user_prep():
    u1 = User(username='Rychlej Ben', email='poustni.had@raraku.edu', role=Role.Client)
    u2 = User(username='Whiskeyjack', email='kolenodobry@palici.mal', role=Role.Therapist)
    u3 = User(username='Šumař', email='struna@ctrnacta.mal', role=Role.Client)
    u4 = User(username='Líto', email='totallynotKotillion@meanas.mal', role=Role.Client)
    u5 = User(username='Dujek', email='levatrochusvedi@houfec.mal', role=Role.Therapist)
    db.session.add_all([u1, u2, u3, u4, u5])
    db.session.commit()


def test_password_hashing():
    user = User(username='Kalam')
    user.set_password('naSedmimestidobry')

    assert user.check_password('naSedmimestidobry') is True
    assert user.check_password('NaSedmimestidobry') is False


def test_alliance_absence(db_setup, user_prep):
    client1 = User.query.filter_by(role=Role.Client).first()
    therapist1 = User.query.filter_by(role=Role.Therapist).first()

    assert client1.clients.all() == []
    assert client1.therapists.all() == []
    assert therapist1.clients.all() == []
    assert therapist1.therapists.all() == []


def test_alliance_creation(db_setup, user_prep):
    client1 = User.query.filter_by(role=Role.Client).first()
    client2 = User.query.filter(User.role == Role.Client).filter(User.id != client1.id).first()
    therapist1 = User.query.filter_by(role=Role.Therapist).first()
    therapist2 = User.query.filter(User.role == Role.Therapist).filter(User.id != therapist1.id).first()

    therapist1.add_client(client1)

    therapist1_clients = therapist1.clients.all()
    client1_therapists = client1.therapists.all()

    assert client1 in therapist1_clients
    assert len(therapist1_clients) is 1
    assert therapist1 in client1_therapists
    assert len(client1_therapists) is 1
    assert client1.is_in_alliance(therapist1) is True
    assert therapist1.is_in_alliance(client1) is True

    assert therapist1.is_in_alliance(therapist1) is False
    assert client1.is_in_alliance(client1) is False
    assert client2.is_in_alliance(therapist1) is False
    assert therapist2.is_in_alliance(client1) is False
    assert therapist1.is_in_alliance(client2) is False


def test_alliance_end(db_setup, user_prep):
    client1 = User.query.filter_by(role=Role.Client).first()
    client2 = User.query.filter(User.role == Role.Client).filter(User.id != client1.id).first()
    therapist1 = User.query.filter_by(role=Role.Therapist).first()
    therapist2 = User.query.filter(User.role == Role.Therapist).filter(User.id != therapist1.id).first()

    assert therapist1.is_in_alliance(client1) is False, "No alliance expected. Check fixture 'db_setup'."

    therapist1.add_client(client1)

    assert therapist1.is_in_alliance(client1) is True
    assert therapist1.is_in_alliance(client2) is False
    assert therapist2.is_in_alliance(client1) is False

    therapist1.break_alliance(client1)

    assert therapist1.is_in_alliance(client1) is False
    assert client1.is_in_alliance(therapist1) is False
    assert therapist1.is_in_alliance(client2) is False
    assert therapist2.is_in_alliance(client1) is False
    assert therapist1.clients.count() == 0
    assert client1.therapists.count() == 0


def test_load_user(db_setup, user_prep):
    user = User.query.first()
    loaded_user = load_user(user.id)

    assert user.id == loaded_user.id
    assert user.email == loaded_user.email


def test_create_alliance_token(db_setup, user_prep):
    therapist = User.query.filter_by(role=Role.Therapist).first()
    client = User.query.filter_by(role=Role.Client).first()

    token = therapist.generate_alliance_offer_token(client)

    alliance = User.verify_alliance_offer_token(token)

    assert therapist == alliance['therapist']
    assert client == alliance['client']


def test_break_alliance_token(db_setup, user_prep):
    therapist = User.query.filter_by(role=Role.Therapist).first()
    client = User.query.filter_by(role=Role.Client).first()

    token = client.generate_break_alliance_token(therapist)

    alliance = User.verify_break_alliance_token(token)

    assert therapist == alliance['therapist']
    assert client == alliance['client']


def test_reset_password_token(db_setup, user_prep):
    user = User.query.first()
    token = user.generate_reset_password_token()
    loaded_user = User.verify_reset_password_token(token)

    assert user == loaded_user


def test_self_destruct_token(db_setup, user_prep):
    user = User.query.first()
    token = user.generate_self_destruct_token()
    loaded_user = User.verify_self_destruct_token(token)

    assert user == loaded_user


def test_token_expired(db_setup, user_prep):
    user = User.query.first()
    token = user.generate_self_destruct_token(expire_in=-1)

    loaded_user = User.verify_self_destruct_token(token)

    assert user != loaded_user
    assert loaded_user is None


def test_token_combined(db_setup, user_prep):
    user = User.query.first()
    token = user.generate_self_destruct_token() + user.generate_reset_password_token()

    loaded_user = User.verify_self_destruct_token(token)
    loaded_user2 = User.verify_reset_password_token(token)

    assert user != loaded_user
    assert loaded_user is None
    assert loaded_user2 is None


def test_token_invalid(db_setup, user_prep):
    user = User.query.first()
    token = user.generate_self_destruct_token() + "No a proto mi říkají Šumař."

    loaded_user = User.verify_self_destruct_token(token)

    assert user != loaded_user
    assert loaded_user is None
